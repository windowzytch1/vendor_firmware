FIRMWARE_IMAGES := \
    abl \
    bluetooth \
    cda \
    cmnlib \
    cmnlib64 \
    devcfg \
    dsp \
    hidden \
    hyp \
    keymaster \
    mdtp \
    mdtpsecapp \
    modem \
    nvdef \
    pmic \
    rpm \
    splash \
    systeminfo \
    tz \
    xbl

AB_OTA_PARTITIONS += $(FIRMWARE_IMAGES)

